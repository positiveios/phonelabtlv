




import UIKit
import AVFoundation


//MARK: - Play Sounds Effect


class SoundsEffect: NSObject {
    
    var player : AVAudioPlayer?
    
    
    
    // MARK: - Play startup music at the beginning

    func playScanBeep(){
        
        let url:NSURL = NSBundle.mainBundle().URLForResource("test", withExtension: "mp3")!
        
        do { player = try AVAudioPlayer(contentsOfURL: url, fileTypeHint: nil) }
        catch let error as NSError { print(error.description) }
        
        player?.numberOfLoops = 0
        player?.prepareToPlay()
        player?.play()
        
        
        
     
        
        //r(contentsOfURL: url!, fileTypeHint: nil)
        
//        player?.prepareToPlay()
//        player?.numberOfLoops = 0
//        player?.volume = 1
//        player?.play()
    }
    
    // MARK: - Stop Playing
    
    func stop(){
        player?.stop()
        self.player = nil
    }
    
    
    
   
    
}
