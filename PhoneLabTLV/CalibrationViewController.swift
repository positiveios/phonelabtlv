//
//  CalibrationViewController.swift
//  PhoneLabTLV
//
//  Created by PositiveApps on 07/05/2016.
//  Copyright © 2016 PositiveApps. All rights reserved.
//

import UIKit
import CoreMotion


class CalibrationViewController: UIViewController {
    
    var currentMaxRotX: Double = 0.0
    var currentMaxRotY: Double = 0.0
    var currentMaxRotZ: Double = 0.0
    
    let motionManager = CMMotionManager()

    @IBOutlet var rotX : UILabel! = nil
    @IBOutlet var rotY : UILabel! = nil
    @IBOutlet var rotZ : UILabel! = nil
    
    @IBOutlet var maxRotX : UILabel! = nil
    @IBOutlet var maxRotY : UILabel! = nil
    @IBOutlet var maxRotZ : UILabel! = nil
    
    
    @IBOutlet weak var RollLabel: UILabel! = nil
    
    @IBOutlet weak var PitchLabel: UILabel! = nil
    
    @IBOutlet weak var YawLabel: UILabel! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
       // self.downloadFile2()

        
        if motionManager.gyroAvailable {
            motionManager.deviceMotionUpdateInterval = 0.2;
            motionManager.startDeviceMotionUpdates()
            
            motionManager.gyroUpdateInterval = 0.2
            
            motionManager.startDeviceMotionUpdatesToQueue(NSOperationQueue.currentQueue()!, withHandler: { (data: CMDeviceMotion?, error : NSError?) in
                self.outputRotationData(data!.rotationRate)
                                if error != nil {
                                    print("\(error)")
                                }

                
                
                
           })
            
//            motionManager.startGyroUpdatesToQueue(NSOperationQueue.currentQueue()!) {
//                [weak self] (gyroData: CMGyroData!, error: NSError!) in
//                
//                self?.outputRotationData(gyroData.rotationRate)
//                if error != nil {
//                    println("\(error)")
//                }
//            }
        
        }else
            
        {
            var alert = UIAlertController(title: "No gyro", message: "Get a Gyro", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        
    }
    
    // radians to degrees
    func radians(fromDegrees degrees: Double) -> Double {
        return 180 * degrees / M_PI
    }
    
    func outputRotationData(rotation:CMRotationRate)
    {
        rotX.text = NSString(format:"Rotation X: %.4f",rotation.x) as String
        if fabs(rotation.x) > fabs(currentMaxRotX)
        {
            currentMaxRotX = rotation.x
        }
        
        rotY.text = NSString(format:"Rotation Y: %.4f", rotation.y) as String
        if fabs(rotation.y) > fabs(currentMaxRotY)
        {
            currentMaxRotY = rotation.y
        }
        rotZ.text = NSString(format:"Rotation Z:%.4f", rotation.z) as String
        if fabs(rotation.z) > fabs(currentMaxRotZ)
        {
            currentMaxRotZ = rotation.z
        }
        
        maxRotX.text = NSString(format:"Max rotation X: %.4f", currentMaxRotX) as String
        maxRotY.text = NSString(format:"Max rotation Y:%.4f", currentMaxRotY) as String
        maxRotZ.text = NSString(format:"Max rotation Z:%.4f", currentMaxRotZ) as String
        
        var attitude = CMAttitude()
        var motion = CMDeviceMotion()
        motion = motionManager.deviceMotion!
        attitude = motion.attitude
        
        
        YawLabel.text = NSString (format: "Yaw: %.2f", attitude.yaw) as String //radians to degress NOT WORKING
        PitchLabel.text = NSString (format: "Pitch: %.2f", attitude.pitch) as String//radians to degress NOT WORKING
        RollLabel.text = NSString (format: "Roll: %.2f", attitude.roll) as String//radians to degress NOT WORKING
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func downloadFile(){
        
        var localPath: NSURL?
        Alamofire.download(.GET,
            "http://download.thinkbroadband.com/10MB.zip",
            destination: { (temporaryURL, response) in
                let directoryURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
                let pathComponent = response.suggestedFilename
                
                localPath = directoryURL.URLByAppendingPathComponent(pathComponent!)
                return localPath!
        })
            .response { (request, response, _, error) in
                print(response)
                //print("Downloaded file to \(localPath!)")
        }
        
    }
    
    
    func downloadFile2(){
        
        
        //let destination = Alamofire.Request.suggestedDownloadDestination(directory: .DocumentDirectory, domain: .UserDomainMask)
        
        let destination = Alamofire.Request.suggestedDownloadDestination(directory: .DocumentDirectory, domain: .UserDomainMask)
        //Alamofire.download(.GET, "https://httpbin.org/stream/100", destination: destination)
        
        Alamofire.download(.GET, "http://download.thinkbroadband.com/1MB.zip", destination: destination)
            .progress { bytesRead, totalBytesRead, totalBytesExpectedToRead in
                print(totalBytesRead)
                
                // This closure is NOT called on the main queue for performance
                // reasons. To update your ui, dispatch to the main queue.
                dispatch_async(dispatch_get_main_queue()) {
                    print("Total bytes read on main queue: \(totalBytesRead)")
                }
            }
            .response { _, _, _, error in
                if let error = error {
                    print("Failed with error: \(error)")
                } else {
                    print("Downloaded file successfully")
                }
        }
        
//        let url = "http://download.thinkbroadband.com/10MB.zip" // this is for example..
//        let destination = Alamofire.Request.suggestedDownloadDestination(directory: .DocumentDirectory, domain: .UserDomainMask)
//        
//        Alamofire.download(.GET, url, parameters: nil, encoding: ParameterEncoding.URL,destination:destination)
//            .progress { bytesRead, totalBytesRead, totalBytesExpectedToRead in
//                // This closure is NOT called on the main queue for performance
//                // reasons. To update your ui, dispatch to the main queue.
//                dispatch_async(dispatch_get_main_queue()) {
//                    // Here you can update your progress object
//                    print("Total bytes read on main queue: \(totalBytesRead)")
//                    print("Progress on main queue: \(Float(totalBytesRead) / Float(totalBytesExpectedToRead))")
//                }
//            }
//            .response { request, _, _, error in
//                print("\(request?.URL)")  // original URL request
//                if let error = error {
//                    let httpError: NSError = error
//                    let statusCode = httpError.code
//                } else { //no errors
//                    let filePath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
//                    print("File downloaded successfully: \(filePath)")
//                }
//        }
        
    }
    

}