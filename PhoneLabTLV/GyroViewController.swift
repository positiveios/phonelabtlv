//
//  GyroViewController.swift
//  PhoneLabTLV
//
//  Created by PositiveApps on 16/05/2016.
//  Copyright © 2016 PositiveApps. All rights reserved.
//

import UIKit
import CoreMotion
import AVFoundation
import AudioToolbox

class GyroViewController: UIViewController {

    
    let manager = CMMotionManager()
    var gyroTest = 0
    var saveRotation : Double = 0
    

    @IBOutlet weak var imagePic: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var initialAttitude = manager.deviceMotion?.attitude
        var showingPrompt = false
        
        // trigger values - a gap so there isn't a flicker zone
        let showPromptTrigger = 1.0
        let showAnswerTrigger = 0.8
        
        
        manager.gyroUpdateInterval = 0.1
        
        manager.startGyroUpdates()
        
        print(Device.version())
        print(Device.type())
        print(Device.size())
        
        if manager.deviceMotionAvailable {
            manager.deviceMotionUpdateInterval = 0.01
            
            manager.startDeviceMotionUpdatesToQueue(NSOperationQueue.mainQueue(), withHandler: { (data : CMDeviceMotion?, error: NSError?) -> Void in
                
                if let gravity = data?.gravity {
                    let rotation = atan2(data!.gravity.x, data!.gravity.y) - M_PI
                    self.imagePic.transform = CGAffineTransformMakeRotation(CGFloat(rotation))
                    
                    
                    if(rotation > -5 && rotation < -4 && self.gyroTest == 0){
                        
                        
                        self.gyroTest = 1
                        
                        let alert = UIAlertView()
                        alert.title = "בדיקת גירוסקופ"
                        alert.message = "בדיקת גירוסקופ עברה בהצלחה"
                        alert.addButtonWithTitle("אישור")
                        alert.show()
                        
                        if let navController = self.navigationController {
                            navController.popViewControllerAnimated(true)
                        }


                    }
                    
                    
                    
                }
                
            })
            
            
        }

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func accelometer(){
        
        if manager.deviceMotionAvailable {
            manager.deviceMotionUpdateInterval = 0.02
            manager.startDeviceMotionUpdatesToQueue(NSOperationQueue.mainQueue()) {
                [weak self] (data: CMDeviceMotion?, error: NSError?) in
                
                if data?.userAcceleration.x < -2.5 {
                    self?.navigationController?.popViewControllerAnimated(true)
                }
            }
        }
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
