//
//  LocalDataManager.swift
//  Veg420
//
//  Created by PositiveApps on 23/05/2016.
//  Copyright © 2016 PositiveApps. All rights reserved.
//

import UIKit

class LocalDataManager{
    
    static var instance: LocalDataManager!
    
    var title = String()
    var message = String()
    var logo = UIImage()
    
    var title2 = String()
    var logo2 = UIImage()
    
    var screenVC : Bool = Bool()
    var brodcastVC : Bool = Bool()
    var gyroVC : Bool = Bool()
    var soundVC : Bool = Bool()
    var dataVC : Bool = Bool()
    
    class func sharedInstance() -> LocalDataManager {
        self.instance = (self.instance ?? LocalDataManager())
        return self.instance
    }
    
    
    func setupTitle(T : String, M : String, image : UIImage){
        
        self.title = T
        
        self.message = M
    
        self.logo = image
    }
    
    
    func setupTitle2(T : String, image : UIImage){
        
        self.title2 = T
        
        self.logo2 = image
    }

    
    func setScreenVC(B : Bool){
        
        self.screenVC = B
        
    }
    
    func setBrodcastVC(B : Bool){
        
        self.brodcastVC = B
        
    }

    func setGyroVC(B : Bool){
        
        self.gyroVC = B
        
    }

    func setSoundVC(B : Bool){
        
        self.soundVC = B
        
    }

    func setDataVC(B : Bool){
        
        self.dataVC = B
        
    }

    



}
