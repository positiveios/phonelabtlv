//
//  MyPopupViewController.swift
//  SLPopupViewControllerDemo
//
//  Created by Nguyen Duc Hoang on 9/13/15.
//  Copyright © 2015 Nguyen Duc Hoang. All rights reserved.
//

import UIKit

protocol SuccessPopDelegate {
    func pressOKSuccess(sender: SuccessPop)
    func pressCancelSuccess(sender: SuccessPop)

    
}
class SuccessPop: UIViewController {
    var delegate:SuccessPopDelegate?
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBAction func btnOK(sender:UIButton) {
        
        
        
        self.delegate?.pressOKSuccess(self)
        
    }
    
    @IBAction func btnCancel(sender:UIButton) {
        self.delegate?.pressCancelSuccess(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        self.titleLabel.text = LocalDataManager.sharedInstance().title2

      //  self.messageLabel.text = LocalDataManager.sharedInstance().message
        
        self.iconImage.image = LocalDataManager.sharedInstance().logo2
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
        /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
