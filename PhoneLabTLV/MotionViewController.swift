//
//  MotionViewController.swift
//  PhoneLabTLV
//
//  Created by PositiveApps on 16/05/2016.
//  Copyright © 2016 PositiveApps. All rights reserved.
//

import UIKit

class MotionViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent?) {
        if motion == .MotionShake {
            
            let alert = UIAlertView()
            alert.title = "בדיקת תזוזה"
            alert.message = "בדיקת תזוזה עברה בהצלחה"
            alert.addButtonWithTitle("אישור")
            alert.show()
            
            if let navController = self.navigationController {
                navController.popViewControllerAnimated(true)
            }

            
            
            
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
