//
//  DownloadUploadViewController.swift
//  PhoneLabTLV
//
//  Created by PositiveApps on 13/07/2016.
//  Copyright © 2016 PositiveApps. All rights reserved.
//

import UIKit

class DownloadUploadViewController: UIViewController ,ConfirmationPopDelegate,SuccessPopDelegate{

    @IBOutlet weak var circularProgressView: KDCircularProgress!
    
    @IBOutlet weak var circularProgressViewUp: KDCircularProgress!

    @IBOutlet weak var connectionLabel: UILabel!
    
    
    var start = 0.0
    var currentCount = 0.0
    let maxCount = 1000000.0 //1048576
    
    
    var currentCountUp = 0.0
    let maxCountUp = 2000000.0
    
    @IBOutlet weak var percentLabel: UILabel!
    @IBOutlet weak var percentLabelUp: UILabel!
    @IBAction func backBtnAction(sender: UIButton) {
        
        
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }

        
        
    }
    
   
      override func viewDidLoad() {
        super.viewDidLoad()
        
        
        do {
            let reachability: Reachability = try Reachability.reachabilityForInternetConnection()
            
            switch reachability.currentReachabilityStatus{
            case .ReachableViaWiFi:
                
                self.connectionLabel.text = "הינך מחובר באמצעות WiFi"
                print("Connected With wifi")
            case .ReachableViaWWAN:
                
                self.connectionLabel.text = "הינך מחובר  3G/4G באמצעות"
                
                print("Connected With Cellular network(3G/4G)")
            case .NotReachable:

                self.connectionLabel.text = "אין חיבור לרשת"
            
            }
        }
        catch let error as NSError{
            print(error.debugDescription)
        }
        
        
        
        self.deletFirst()
        self.deletSecound()
        
        self.view.backgroundColor = UIColor.init(colorCodeInHex: "1e9e49")
        
        self.displayViewController(.BottomTop)

        
        self.percentLabel.text = "0%"
        
        self.percentLabelUp.text = "0%"
        

        // Do any additional setup after loading the view, typically from a nib.
        circularProgressView.angle = 0
        circularProgressViewUp.angle = 0

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func increaseProgressButtonTapped(sender: UIButton) {
        if currentCount != maxCount {
            currentCount += 0.5
            let newAngleValue = newAngle()
            
            circularProgressView.animateToAngle(newAngleValue, duration: 0.5, completion: nil)
        }
    }
    
    func newAngle() -> Int {
        return Int(360 * (currentCount / maxCount))
    }
    
    func newAngleUp() -> Int {
        return Int(360 * (currentCountUp / maxCountUp))
    }

    
    @IBAction func resetButtonTapped(sender: UIButton) {
        currentCount = 0
        circularProgressView.animateFromAngle(circularProgressView.angle, toAngle: 0, duration: 0.5, completion: nil)
    }

    
    
    func displayViewController(animationType: SLpopupViewAnimationType) {
        
        let imageLogo = UIImage(named: "transmission_before_btn")
        
        LocalDataManager.sharedInstance().setupTitle("בדיקת קליטה ושידור", M:  "המכשיר שלך יבצע כעת בדיקה של טעינת קובץ ושליחת קובץ", image: imageLogo!)
        
        let myPopupViewController:ConfirmationPop = ConfirmationPop(nibName:"ConfirmationPop", bundle: nil)
        myPopupViewController.delegate = self
        self.presentpopupViewController(myPopupViewController, animationType: animationType, completion: { () -> Void in
            
        })
    }
    
    
    func displayViewControllerSuccess(animationType: SLpopupViewAnimationType) {
        
        let image = UIImage(named: "transmission_after_btn")
        
        LocalDataManager.sharedInstance().setupTitle2("הבדיקה הושלמה בהצלחה!", image: image!)
        let myPopupViewController:SuccessPop = SuccessPop(nibName:"SuccessPop", bundle: nil)
        myPopupViewController.delegate = self
        self.presentpopupViewController2(myPopupViewController, animationType: animationType, completion: { () -> Void in
            
        })
    }

    
    func pressOK(sender: ConfirmationPop) {
        
        self.dismissPopupViewController(.Fade)
        
        
        //        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("tryAgain")
        //        self.presentViewController(vc!, animated: true, completion: nil)
        self.downloadFile2()

        
    }
    
    func pressCancel(sender: ConfirmationPop) {
        
        
        self.dismissPopupViewController(.Fade)

        
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }

        
    }

    
    func pressOKSuccess(sender: SuccessPop) {
        
        
        LocalDataManager.sharedInstance().setBrodcastVC(true)

        self.dismissPopupViewController2(.Fade)
        
        
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
        
        
    }
    
    func pressCancelSuccess(sender: SuccessPop) {
        self.dismissPopupViewController(.Fade)
        
        
        
        
    }

    func deletFirst(){
        
        let path = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        let filePath = url.URLByAppendingPathComponent("1MB.zip").path!
        let fileManager = NSFileManager.defaultManager()
        if fileManager.fileExistsAtPath(filePath) {
            print("FILE AVAILABLE")
            
            do {
                try fileManager.removeItemAtPath(filePath)
            } catch let error as NSError {
                print(error.debugDescription)
            }
            //    fileManager.removeItemAtPath("1MB.zip")
            
        } else {
            print("FILE NOT AVAILABLE")
        }
        

        
        
    }
    func deletSecound(){
        
        let path = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        let filePath = url.URLByAppendingPathComponent("2MB.zip").path!
        let fileManager = NSFileManager.defaultManager()
        if fileManager.fileExistsAtPath(filePath) {
            print("FILE AVAILABLE")
            
            do {
                try fileManager.removeItemAtPath(filePath)
            } catch let error as NSError {
                print(error.debugDescription)
            }
            //    fileManager.removeItemAtPath("1MB.zip")
            
        } else {
            print("FILE NOT AVAILABLE")
        }
        
        
        
        
    }

    func downloadFile2(){
        
        
        
        let destination = Alamofire.Request.suggestedDownloadDestination(directory: .DocumentDirectory, domain: .UserDomainMask)
        //Alamofire.download(.GET, "https://httpbin.org/stream/100", destination: destination)
        
        Alamofire.download(.GET, "http://download.thinkbroadband.com/1MB.zip", destination: destination)
            .progress { bytesRead, totalBytesRead, totalBytesExpectedToRead in
                print(totalBytesRead)
                
                // This closure is NOT called on the main queue for performance
                // reasons. To update your ui, dispatch to the main queue.
                dispatch_async(dispatch_get_main_queue()) {
                    print("Total bytes read on main queue: \(totalBytesRead)")
                    
                    let progressBar : Double = Double(totalBytesRead)
                    
                    //self.start -= progressBar
                    
                    
                    
                    if self.currentCount < self.maxCount {
                        self.currentCount = progressBar
                        let newAngleValue = self.newAngle()
                        
                        self.percentLabel.text = String("\(totalBytesRead / 10000)%")
                        self.circularProgressView.animateToAngle(newAngleValue, duration: 0.5, completion: nil)
                    }else{
                        
                        
                        //self.currentCount = 1048576.0
                        self.circularProgressView.animateFromAngle(self.circularProgressView.angle, toAngle: 360, duration: 0.5, completion: nil)
                        
                        self.percentLabel.text = "100%"
                        
                        

                    }

                    
                }
            }
            .response { _, _, _, error in
                if let error = error {
                    print("Failed with error: \(error)")

                } else {
                    self.downloadFile()

                    print("Downloaded file successfully")
                }
        }
        
        
    }
    func downloadFile(){
        
        
        
        let destination = Alamofire.Request.suggestedDownloadDestination(directory: .DocumentDirectory, domain: .UserDomainMask)
        //Alamofire.download(.GET, "https://httpbin.org/stream/100", destination: destination)
        
        Alamofire.download(.GET, "http://download.thinkbroadband.com/2MB.zip", destination: destination)
            .progress { bytesRead, totalBytesRead, totalBytesExpectedToRead in
                print(totalBytesRead)
                
                // This closure is NOT called on the main queue for performance
                // reasons. To update your ui, dispatch to the main queue.
                dispatch_async(dispatch_get_main_queue()) {
                    print("Total bytes read on main queue: \(totalBytesRead)")
                    
                    let progressBar : Double = Double(totalBytesRead)
                    
                    //self.start -= progressBar
                    
                    
                    
                    if self.currentCountUp < self.maxCountUp {
                        self.currentCountUp = progressBar
                        let newAngleValue = self.newAngleUp()
                        
                        self.percentLabelUp.text = String("\(totalBytesRead / 20000)%")
                        self.circularProgressViewUp.animateToAngle(newAngleValue, duration: 0.5, completion: nil)
                    }else{
                        
                        
                        //self.currentCount = 1048576.0
                        self.circularProgressViewUp.animateFromAngle(self.circularProgressViewUp.angle, toAngle: 360, duration: 0.5, completion: nil)
                        
                        self.percentLabelUp.text = "100%"
                        
                        
                        
                    }
                    
                    
                }
            }
            .response { _, _, _, error in
                if let error = error {
                    print("Failed with error: \(error)")
                } else {
                    print("Downloaded file successfully")
                    
                    
                    self.displayViewControllerSuccess(.BottomTop)
                    
                }
        }
        
        
    }


    
}
