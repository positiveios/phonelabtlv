//
//  PaymentViewController.swift
//  PhoneLabTLV
//
//  Created by PositiveApps on 29/07/2016.
//  Copyright © 2016 PositiveApps. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController,UITextFieldDelegate {

    let dropDown = DropDown()
    let dropDown2 = DropDown()

    @IBOutlet weak var cvvText: UITextField!
    @IBOutlet weak var idText: UITextField!
    @IBOutlet weak var creditCardText: UITextField!
    @IBOutlet weak var yearBtn: UIButton!
    var yearArray = [String]()
    var mounthArray = [String]()
    
    @IBOutlet weak var monthBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.yearBtn.setTitleColor(UIColor.init(colorCodeInHex: "1f7134"), forState: UIControlState.Normal)
        
        
       self.yearBtn.titleLabel!.font = UIFont(name: "Rubik-Bold", size: 19)

        
        self.monthBtn.setTitleColor(UIColor.init(colorCodeInHex: "1f7134"), forState: UIControlState.Normal)
        
        
        self.creditCardText.textColor = UIColor.init(colorCodeInHex: "1f7134")
        self.idText.textColor = UIColor.init(colorCodeInHex: "1f7134")
        self.cvvText.textColor = UIColor.init(colorCodeInHex: "1f7134")

        
        
        self.monthBtn.titleLabel!.font = UIFont(name: "Rubik-Bold", size: 19)
        
        self.mounthArray.append("01")
        self.mounthArray.append("02")
        self.mounthArray.append("03")
        self.mounthArray.append("04")
        self.mounthArray.append("05")
        self.mounthArray.append("06")
        self.mounthArray.append("07")
        self.mounthArray.append("08")
        self.mounthArray.append("09")
        self.mounthArray.append("10")
        self.mounthArray.append("11")
        self.mounthArray.append("12")

        
        
        self.yearArray.append("2016")
        self.yearArray.append("2017")
        self.yearArray.append("2018")
        self.yearArray.append("2019")
        self.yearArray.append("2021")
        self.yearArray.append("2022")
        self.yearArray.append("2023")
        self.yearArray.append("2024")
        self.yearArray.append("2025")
        self.yearArray.append("2026")
        self.yearArray.append("2027")
        self.yearArray.append("2028")
        self.yearArray.append("2029")
        self.yearArray.append("2030")
        self.yearArray.append("2031")

        
        dropDown.dataSource = yearArray
        
        dropDown2.dataSource = mounthArray

        dropDown.selectionAction = { [unowned self] (index, item) in
            
            //self.messageText.resignFirstResponder()
            
            self.yearBtn.setTitle(item, forState: .Normal)
            
            print(item)
            
          //  self.productID = LocalDataManager.sharedInstance().productID[index]
            
            //self.subject = item
        }
        
        
        dropDown.anchorView = yearBtn
        dropDown.bottomOffset = CGPoint(x: 0, y:yearBtn.bounds.height)
        
        
        
        
        dropDown2.selectionAction = { [unowned self] (index, item) in
            
            //self.messageText.resignFirstResponder()
            
            self.monthBtn.setTitle(item, forState: .Normal)
            
            print(item)
            
            //  self.productID = LocalDataManager.sharedInstance().productID[index]
            
            //self.subject = item
        }
        
        
        dropDown2.anchorView = monthBtn
        dropDown2.bottomOffset = CGPoint(x: 0, y:monthBtn.bounds.height)
        
        self.view.backgroundColor = UIColor.init(colorCodeInHex: "1e9e49")


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showOrDismiss(sender: AnyObject) {
      
        self.creditCardText.resignFirstResponder()
        self.cvvText.resignFirstResponder()
        self.idText.resignFirstResponder()

        
        if dropDown.hidden {
            dropDown.show()
        } else {
            dropDown.hide()
        }
    }

    @IBAction func showOrDismiss2(sender: AnyObject) {
        
        self.creditCardText.resignFirstResponder()
        self.cvvText.resignFirstResponder()
        self.idText.resignFirstResponder()

        
        if dropDown2.hidden {
            dropDown2.show()
        } else {
            dropDown2.hide()
        }
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        
        self.creditCardText.resignFirstResponder()
        self.cvvText.resignFirstResponder()
        self.idText.resignFirstResponder()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
