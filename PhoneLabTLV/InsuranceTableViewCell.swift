//
//  InsuranceTableViewCell.swift
//  PhoneLabTLV
//
//  Created by PositiveApps on 27/07/2016.
//  Copyright © 2016 PositiveApps. All rights reserved.
//

import UIKit

class InsuranceTableViewCell: UITableViewCell {

    @IBOutlet weak var VCLowTitleLabel: UILabel!
    @IBOutlet weak var insuranceBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
