//
//  ViewController.swift
//  tuoch
//
//  Created by PositiveApps on 04/07/2016.
//  Copyright © 2016 PositiveApps. All rights reserved.
//

import UIKit

class TouchViewController: UIViewController,ConfirmationPopDelegate,SuccessPopDelegate {

    @IBOutlet weak var ball1Image: UIImageView!
    var completion: (() -> ())?

    let movmentP : CGFloat = 2
    let movmentM : CGFloat = -2

    //  MARK: - Layout

    @IBAction func backBtnAction(sender: UIButton) {
        
        
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
        
        
        
    }

    
    @IBOutlet weak var dragAreaView: UIView!
    @IBOutlet weak var dragView: UIView!
    @IBOutlet weak var goalView: UIView!
    @IBOutlet var panGesture: UIPanGestureRecognizer!

    @IBOutlet weak var dragAreaView2: UIView!
    @IBOutlet weak var dragView2: UIView!
    @IBOutlet weak var goalView2: UIView!
    @IBOutlet var panGesture2: UIPanGestureRecognizer!

    @IBOutlet weak var dragAreaView3: UIView!
    @IBOutlet weak var dragView3: UIView!
    @IBOutlet weak var goalView3: UIView!
    @IBOutlet var panGesture3: UIPanGestureRecognizer!
    
    @IBOutlet weak var dragAreaView4: UIView!
    @IBOutlet weak var dragView4: UIView!
    @IBOutlet weak var goalView4: UIView!
    @IBOutlet var panGesture4: UIPanGestureRecognizer!
    
    @IBOutlet weak var dragAreaView5: UIView!
    @IBOutlet weak var dragView5: UIView!
    @IBOutlet weak var goalView5: UIView!
    @IBOutlet var panGesture5: UIPanGestureRecognizer!
    
    //  MARK: - panfunc

    
    @IBAction func panfunc(sender: UIPanGestureRecognizer) {
        
        self.panGesture.enabled = true
        
        sender.locationInView(dragAreaView)
        var velocity = sender.velocityInView(view)
        var translation = sender.translationInView(view)
        
        //sender.locationOfTouch(2, inView: self.dragAreaView)
        
        if sender.state == UIGestureRecognizerState.Began {
            print("Gesture began")
        } else if sender.state == UIGestureRecognizerState.Changed {
            print("Gesture is changing")
        } else if sender.state == UIGestureRecognizerState.Ended {
            
            if self.isGoalReached {
                
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)

                
                if let completion = self.completion {
                    completion()

                }
            }
            else {
                self.returnToStartLocationAnimated(true)
            }

            
            self.allGoalReaced()
            

            
            print("Gesture ended")
        }
        if sender.state == UIGestureRecognizerState.Began || sender.state == UIGestureRecognizerState.Changed {
            
            let translation = sender.translationInView(self.dragAreaView)
            // note: 'view' is optional and need to be unwrapped
            if(translation.x > self.movmentP) || (translation.x <  self.movmentM){
                
                
                self.panGesture.enabled = false
                
                self.returnToStartLocationAnimated(true)
                

                
                print("OK")
                
                
            }
            
            
            sender.view!.center = CGPointMake(sender.view!.center.x, sender.view!.center.y + translation.y)
            sender.setTranslation(CGPointMake(0,0), inView: self.dragAreaView)
        }

    }
    
    
    @IBAction func panfunc2(sender: UIPanGestureRecognizer) {
        
        self.panGesture2.enabled = true
        
        sender.locationInView(dragAreaView2)
        var velocity = sender.velocityInView(view)
        var translation = sender.translationInView(view)
        
        
        
        if sender.state == UIGestureRecognizerState.Began {
            print("Gesture began")
        } else if sender.state == UIGestureRecognizerState.Changed {
            print("Gesture is changing")
        } else if sender.state == UIGestureRecognizerState.Ended {
            
            if self.isGoalReached2 {
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)

                if let completion = self.completion {
                    completion()
                }
            }
            else {
                self.returnToStartLocationAnimated2(true)
            }
            
            
            self.allGoalReaced()

            
            print("Gesture ended")
        }
        if sender.state == UIGestureRecognizerState.Began || sender.state == UIGestureRecognizerState.Changed {
            
            let translation = sender.translationInView(self.dragAreaView)
            // note: 'view' is optional and need to be unwrapped
            if(translation.x > self.movmentP) || (translation.x < self.movmentM){
                
                
                self.panGesture2.enabled = false
                
                self.returnToStartLocationAnimated2(true)
                

                
                print("OK")
                
                
            }
            
            
            sender.view!.center = CGPointMake(sender.view!.center.x, sender.view!.center.y + translation.y)
            sender.setTranslation(CGPointMake(0,0), inView: self.dragAreaView2)
        }
        
    }

    @IBAction func panfunc3(sender: UIPanGestureRecognizer) {
        
        self.panGesture3.enabled = true
        
        sender.locationInView(dragAreaView3)
        var velocity = sender.velocityInView(view)
        var translation = sender.translationInView(view)
        
        //sender.locationOfTouch(2, inView: self.dragAreaView)
        
        if sender.state == UIGestureRecognizerState.Began {
            print("Gesture began")
        } else if sender.state == UIGestureRecognizerState.Changed {
            print("Gesture is changing")
        } else if sender.state == UIGestureRecognizerState.Ended {
            
            if self.isGoalReached3 {
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)

                if let completion = self.completion {
                    completion()
                }
            }
            else {
                self.returnToStartLocationAnimated3(true)
            }
            
            
            self.allGoalReaced()

            
            print("Gesture ended")
        }
        if sender.state == UIGestureRecognizerState.Began || sender.state == UIGestureRecognizerState.Changed {
            
            let translation = sender.translationInView(self.dragAreaView3)
            // note: 'view' is optional and need to be unwrapped
            if(translation.x > self.movmentP) || (translation.x < self.movmentM){
                
                
                self.panGesture3.enabled = false
                
                self.returnToStartLocationAnimated3(true)
                

                
                print("OK")
                
                
            }
            
            
            sender.view!.center = CGPointMake(sender.view!.center.x, sender.view!.center.y + translation.y)
            sender.setTranslation(CGPointMake(0,0), inView: self.dragAreaView3)
        }
        
    }

    @IBAction func panfunc4(sender: UIPanGestureRecognizer) {
        
        self.panGesture4.enabled = true
        
        sender.locationInView(dragAreaView4)
        var velocity = sender.velocityInView(view)
        var translation = sender.translationInView(view)
        
        //sender.locationOfTouch(2, inView: self.dragAreaView)
        
        if sender.state == UIGestureRecognizerState.Began {
            print("Gesture began")
        } else if sender.state == UIGestureRecognizerState.Changed {
            print("Gesture is changing")
        } else if sender.state == UIGestureRecognizerState.Ended {
            
            if self.isGoalReached4 {
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)

                if let completion = self.completion {
                    completion()
                }
            }
            else {
                self.returnToStartLocationAnimated4(true)
            }
            
            
            self.allGoalReaced()

            
            print("Gesture ended")
        }
        if sender.state == UIGestureRecognizerState.Began || sender.state == UIGestureRecognizerState.Changed {
            
            let translation = sender.translationInView(self.dragAreaView4)
            // note: 'view' is optional and need to be unwrapped
            if(translation.x > self.movmentP) || (translation.x < self.movmentM){
                
                
                self.panGesture4.enabled = false
                
                self.returnToStartLocationAnimated4(true)
                
                

                print("OK")
                
                
            }
            
            
            sender.view!.center = CGPointMake(sender.view!.center.x, sender.view!.center.y + translation.y)
            sender.setTranslation(CGPointMake(0,0), inView: self.dragAreaView4)
        }
        
    }

    @IBAction func panfunc5(sender: UIPanGestureRecognizer) {
        
        self.panGesture5.enabled = true
        
        sender.locationInView(dragAreaView5)
        var velocity = sender.velocityInView(view)
        var translation = sender.translationInView(view)
        
        //sender.locationOfTouch(2, inView: self.dragAreaView)
        
        if sender.state == UIGestureRecognizerState.Began {
            print("Gesture began")
        } else if sender.state == UIGestureRecognizerState.Changed {
            print("Gesture is changing")
        } else if sender.state == UIGestureRecognizerState.Ended {
            
            if self.isGoalReached5 {
                
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)

                
                if let completion = self.completion {
                    completion()
                }
            }
            else {
                self.returnToStartLocationAnimated(true)
            }
            
            self.allGoalReaced()

            
            
            print("Gesture ended")
        }
        if sender.state == UIGestureRecognizerState.Began || sender.state == UIGestureRecognizerState.Changed {
            
            let translation = sender.translationInView(self.dragAreaView)
            // note: 'view' is optional and need to be unwrapped
            if(translation.x > self.movmentP) || (translation.x < self.movmentM){
                
                
                self.panGesture5.enabled = false
                
                self.returnToStartLocationAnimated5(true)
                
                
                
                print("OK")
                
                
            }
            
            
            sender.view!.center = CGPointMake(sender.view!.center.x, sender.view!.center.y + translation.y)
            sender.setTranslation(CGPointMake(0,0), inView: self.dragAreaView5)
        }
        
    }

    
    
//    @IBAction func handlePan(gestureRecognizer: UIPanGestureRecognizer) {
//        if gestureRecognizer.state == UIGestureRecognizerState.Began || gestureRecognizer.state == UIGestureRecognizerState.Changed {
//            
//            let translation = gestureRecognizer.translationInView(self.dragAreaView)
//            // note: 'view' is optional and need to be unwrapped
//            gestureRecognizer.view!.center = CGPointMake(gestureRecognizer.view!.center.x + translation.x, gestureRecognizer.view!.center.y + translation.y)
//            gestureRecognizer.setTranslation(CGPointMake(0,0), inView: self.dragAreaView)
//        }  
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.displayViewController(.BottomTop)

        self.view.backgroundColor = UIColor.init(colorCodeInHex: "1e9e49")

        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //  MARK: - GoalReached

    
    var isGoalReached: Bool {
        get {
            let distanceFromGoal: CGFloat = sqrt(pow(self.dragView.center.x - self.goalView.center.x, 1) + pow(self.dragView.center.y - self.goalView.center.y, 1))
            return distanceFromGoal < self.dragView.bounds.size.width 
        }
    }
    var isGoalReached2: Bool {
        get {
            let distanceFromGoal: CGFloat = sqrt(pow(self.dragView2.center.x - self.goalView2.center.x, 1) + pow(self.dragView2.center.y - self.goalView2.center.y, 1))
            return distanceFromGoal < self.dragView2.bounds.size.width
        }
    }
    var isGoalReached3: Bool {
        get {
            let distanceFromGoal: CGFloat = sqrt(pow(self.dragView3.center.x - self.goalView3.center.x, 1) + pow(self.dragView3.center.y - self.goalView3.center.y, 1))
            return distanceFromGoal < self.dragView3.bounds.size.width
        }
    }
    var isGoalReached4: Bool {
        get {
            let distanceFromGoal: CGFloat = sqrt(pow(self.dragView4.center.x - self.goalView4.center.x, 1) + pow(self.dragView4.center.y - self.goalView4.center.y, 1))
            return distanceFromGoal < self.dragView4.bounds.size.width
        }
    }
    var isGoalReached5: Bool {
        get {
            let distanceFromGoal: CGFloat = sqrt(pow(self.dragView5.center.x - self.goalView5.center.x, 1) + pow(self.dragView5.center.y - self.goalView5.center.y, 1))
            return distanceFromGoal < self.dragView5.bounds.size.width
        }
    }



    
    //  MARK: - returnToStartLocationAnimated

    
    func returnToStartLocationAnimated(animated: Bool) {
        if (animated) {
            UIView.animateWithDuration(
                0.3,
                delay: 0,
                options: .BeginFromCurrentState,
                animations: { () -> Void in
                    self.dragView.frame.origin.x = self.dragView.frame.origin.x
                    self.dragView.frame.origin.y = 0
                    
                },
                completion: nil)
        }
    }
    
    func returnToStartLocationAnimated2(animated: Bool) {
        //        self.dragViewX.constant = self.dragAreaView.bounds.size.width / 6 - 25//(self.dragAreaView.bounds.size.width - self.dragView.bounds.size.width) / 2
        //        self.dragViewY.constant = 0//self.initialDragViewY
        
        if (animated) {
            UIView.animateWithDuration(
                0.3,
                delay: 0,
                options: .BeginFromCurrentState,
                animations: { () -> Void in
                    self.dragView2.frame.origin.x = self.dragView2.frame.origin.x
                    self.dragView2.frame.origin.y = 0
                    
                },
                completion: nil)
        }
    }

    func returnToStartLocationAnimated3(animated: Bool) {
        if (animated) {
            UIView.animateWithDuration(
                0.3,
                delay: 0,
                options: .BeginFromCurrentState,
                animations: { () -> Void in
                    self.dragView3.frame.origin.x = self.dragView3.frame.origin.x
                    self.dragView3.frame.origin.y = 0
                    
                },
                completion: nil)
        }
    }
    func returnToStartLocationAnimated4(animated: Bool) {
        if (animated) {
            UIView.animateWithDuration(
                0.3,
                delay: 0,
                options: .BeginFromCurrentState,
                animations: { () -> Void in
                    self.dragView4.frame.origin.x = self.dragView4.frame.origin.x
                    self.dragView4.frame.origin.y = 0
                    
                },
                completion: nil)
        }
    }
    func returnToStartLocationAnimated5(animated: Bool) {
        if (animated) {
            UIView.animateWithDuration(
                0.3,
                delay: 0,
                options: .BeginFromCurrentState,
                animations: { () -> Void in
                    self.dragView5.frame.origin.x = self.dragView5.frame.origin.x
                    self.dragView5.frame.origin.y = 0
                    
                },
                completion: nil)
        }
    }

    
    func displayViewController(animationType: SLpopupViewAnimationType) {
        
        let image = UIImage(named: "screen_before_btn")
        
        LocalDataManager.sharedInstance().setupTitle("בדיקת מסך", M:  "גרור את כל העיגולים מלמעלה למטה",image: image!)
        
        let myPopupViewController:ConfirmationPop = ConfirmationPop(nibName:"ConfirmationPop", bundle: nil)
        myPopupViewController.delegate = self
        self.presentpopupViewController(myPopupViewController, animationType: animationType, completion: { () -> Void in
            
        })
    }
    
    
    
    func displayViewControllerSuccess(animationType: SLpopupViewAnimationType) {
        
        let image = UIImage(named: "screen_after_btn")
        
        LocalDataManager.sharedInstance().setupTitle2("הבדיקה הושלמה בהצלחה!", image: image!)
        let myPopupViewController:SuccessPop = SuccessPop(nibName:"SuccessPop", bundle: nil)
        myPopupViewController.delegate = self
        self.presentpopupViewController2(myPopupViewController, animationType: animationType, completion: { () -> Void in
            
        })
    }
    
    func pressOK(sender: ConfirmationPop) {
        
        self.dismissPopupViewController(.Fade)
        
        
        //        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("tryAgain")
        //        self.presentViewController(vc!, animated: true, completion: nil)
        
        
    }
    
    func pressCancel(sender: ConfirmationPop) {
        self.dismissPopupViewController(.Fade)
        
        
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
        
        
    }
    
    
    
    func pressOKSuccess(sender: SuccessPop) {
        
        
        LocalDataManager.sharedInstance().setScreenVC(true)
        
        self.dismissPopupViewController2(.Fade)
        
        
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
        
        
    }
    
    func pressCancelSuccess(sender: SuccessPop) {
        self.dismissPopupViewController(.Fade)
        
        
        
        
    }

    
    
    func allGoalReaced(){
        
        if(self.isGoalReached && self.isGoalReached2 && self.isGoalReached3 && self.isGoalReached4 && self.isGoalReached5){
            
            
            //  self.displayViewController(.BottomTop)
            
            self.displayViewControllerSuccess(.BottomTop)
            
        }
        

    }
    
    
}

