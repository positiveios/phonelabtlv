//
//  BallMotionViewController.swift
//  PhoneLabTLV
//
//  Created by PositiveApps on 19/06/2016.
//  Copyright © 2016 PositiveApps. All rights reserved.
//

import UIKit
import CoreMotion

class BallMotionViewController: UIViewController,ConfirmationPopDelegate,SuccessPopDelegate {

    
    @IBOutlet weak var ballImage2: UIImageView!
    @IBOutlet weak var ballImage1: UIImageView!
    @IBOutlet weak var goalView5: UIView!
    @IBOutlet weak var goalView4: UIView!

    var isGoal1 : Bool = false
    var isGoal2 : Bool = false

    var vib1 = 0
    var vib2 = 0
    
    var isGoalReached5: Bool {
        get {
            let distanceFromGoal: CGFloat = sqrt(pow(self.ball.center.x - self.goalView5.center.x, 2) + pow(self.ball.center.y - self.goalView5.center.y, 2))
            return distanceFromGoal < self.ball.bounds.size.width / 2
        }
    }

    var isGoalReached4: Bool {
        get {
            let distanceFromGoal: CGFloat = sqrt(pow(self.ball.center.x - self.goalView4.center.x, 2) + pow(self.ball.center.y - self.goalView4.center.y, 2))
            return distanceFromGoal < self.ball.bounds.size.width / 2
        }
    }

    
    
    let manager = CMMotionManager()
    var X : Float = 0
    var Y : Float = 0
    var R : Float = 40
    
    var once = 0
    
    let ball = UIView(frame: CGRect(x: 160, y: 250, width: 55, height: 55))
    var timerUpdate:NSTimer = NSTimer()

    let imageBall = UIImage(named: "gyro_tst_ball")
    
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 55, height: 55))
    
    
    @IBAction func backBtnAction(sender: UIButton) {
        
        
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
        
        
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
         self.displayViewController(.BottomTop)

        
        self.view.backgroundColor = UIColor.init(colorCodeInHex: "1e9e49")

        
        
        self.imageView.image = imageBall
        
        ball.addSubview(imageView)
        
        
       // ball.layer.cornerRadius = 20
        
       // ball.backgroundColor = UIColor.redColor()
        
        self.view.addSubview(ball)
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.ballImage1.hidden = true
        self.ballImage2.hidden = true

        self.timerUpdate = NSTimer.scheduledTimerWithTimeInterval(0.05, target: self, selector: #selector(self.updateDiviceMotion), userInfo: nil, repeats: true)

        
        
        self.manager.deviceMotionUpdateInterval = 1.0 / 60.0
        
        self.manager.startDeviceMotionUpdatesUsingReferenceFrame(CMAttitudeReferenceFrame.XArbitraryCorrectedZVertical)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func updateBall(roll : Float ,Pitch : Float,Yaw : Float,accX : Float,accY : Float,accZ : Float){
        
        
        self.X += 2 * roll
        self.Y += 2 * Pitch

        self.X *= 0.8
        self.Y *= 0.8
        
        var newX = Float(self.ball.frame.origin.x) + X
        var newY = Float(self.ball.frame.origin.y) + Y

        
        let height = Float(self.view.bounds.height) - 50
        let width = Float(self.view.bounds.width) - 50
        print(height,width)
        
        
        newX = fmin(width, fmax(0, newX))
        newY = fmin(height, fmax(64, newY))
        
//        let minFloatY = Float(self.view.frame.width / 2 - 20)
//        var minFloatX = Float(self.view.frame.width / 2 - 20)
        
        
        let newR = R + 10 * accZ
        
        self.ball.frame = CGRect(x: CGFloat(newX), y: CGFloat(newY), width: CGFloat(newR), height: CGFloat(newR))
        
        //print("newX \(newX)")
//print("newY \(newY)")
        
//        if(newY > minFloatY - 2 && newY < minFloatY + 2) && (newX >  138 && newX < 142) && (self.once == 0){
//            
//            self.once = 1
//            
//            manager.stopGyroUpdates()
//            
//            manager.stopDeviceMotionUpdates()
//            
//            manager.stopMagnetometerUpdates()
//            
//            if let navController = self.navigationController {
//                navController.popViewControllerAnimated(true)
//            }
//            
//            
//            
//            let alert = UIAlertView()
//            alert.title = "בדיקת גירוסקופ"
//            alert.message = "בדיקת גירוסקופ עברה בהצלחה"
//            alert.addButtonWithTitle("אישור")
//            alert.show()
//            //
//            
//        }

    }
    
    func updateDiviceMotion(){
        
        
        if self.isGoalReached5 {
          
            
            if(self.vib2 == 0){
                
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
                
                self.vib2 += 1
            }

            
            self.isGoal1 = true
            self.ballImage2.hidden = false

        
        }
        else {
            
        
        }
        
        
        if self.isGoalReached4 {
            
            if(self.vib1 == 0){
                
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)

                self.vib1 += 1
            }
            

            self.isGoal2 = true
            self.ballImage1.hidden = false


            
        }
        else {
            
            
        }


        
        if(self.isGoal1 && self.isGoal2){
            
            self.isGoal1 = false
            
            self.isGoal2 = false
            
          //  self.displayViewController(.BottomTop)

            self.displayViewControllerSuccess(.BottomTop)
            
        }
        
        
        
        let deviceMotion = self.manager.deviceMotion
        
        if(deviceMotion == nil){
            
            return
        }
        
        let attitude = deviceMotion?.attitude
        
        let userAcceleration = deviceMotion?.userAcceleration
        
        
        let roll : Float = Float(attitude!.roll)
        let pitch  : Float = Float(attitude!.pitch)
        let yaw  : Float = Float(attitude!.yaw)

        
        let accX  : Float = Float(userAcceleration!.x)
        let accY : Float  = Float(userAcceleration!.y)
        let accZ  : Float = Float(userAcceleration!.z)

//        
//        print("accX \(accX)")
//        print("accY \(accY)")
//        print("accZ \(accZ)")
        
        self.updateBall(roll, Pitch: pitch, Yaw: yaw, accX: accX, accY: accY, accZ: accZ)
        
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func displayViewController(animationType: SLpopupViewAnimationType) {
        
        let image = UIImage(named: "gyro_before_btn")
        
        LocalDataManager.sharedInstance().setupTitle("בדיקת GYRO", M:  "השכב את המכשיר והטה אותו כך שהכדור יתגלגל ויכנס לשני החורים",image: image!)
        
        let myPopupViewController:ConfirmationPop = ConfirmationPop(nibName:"ConfirmationPop", bundle: nil)
        myPopupViewController.delegate = self
        self.presentpopupViewController(myPopupViewController, animationType: animationType, completion: { () -> Void in
            
        })
    }
    
    
    
    func displayViewControllerSuccess(animationType: SLpopupViewAnimationType) {
        
        let image = UIImage(named: "gyro_after_btn")
        
        LocalDataManager.sharedInstance().setupTitle2("הבדיקה הושלמה בהצלחה!", image: image!)
        let myPopupViewController:SuccessPop = SuccessPop(nibName:"SuccessPop", bundle: nil)
        myPopupViewController.delegate = self
        self.presentpopupViewController2(myPopupViewController, animationType: animationType, completion: { () -> Void in
            
        })
    }

    func pressOK(sender: ConfirmationPop) {
        
        self.dismissPopupViewController(.Fade)
        
        
//        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("tryAgain")
//        self.presentViewController(vc!, animated: true, completion: nil)
        
        
    }
    
    func pressCancel(sender: ConfirmationPop) {
        self.dismissPopupViewController(.Fade)

        
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }

        
    }


    
    func pressOKSuccess(sender: SuccessPop) {
        
        
        LocalDataManager.sharedInstance().setGyroVC(true)
        //LocalDataManager.sharedInstance().setScreenVC(true)
        
        
        self.dismissPopupViewController2(.Fade)
        
        
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
        
        
    }
    
    func pressCancelSuccess(sender: SuccessPop) {
        self.dismissPopupViewController(.Fade)
        
        
        
        
    }

    
    func setupTitle(sender: ConfirmationPop) {
        
    
        
        
    }

    
}
