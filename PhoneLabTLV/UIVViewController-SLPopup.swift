//
//  UIVViewController-SLpopup.swift
//  AnonyChat
//
//  Created by Nguyen Duc Hoang on 9/6/15.
//  Copyright © 2015 Home. All rights reserved.
//

import UIKit
import QuartzCore
import ObjectiveC

enum SLpopupViewAnimationType: Int {
    case BottomTop
    case TopBottom
    case BottomBottom
    case TopTop
    case LeftLeft
    case LeftRight
    case RightLeft
    case RightRight
    case Fade
}
let kSourceViewTag = 11111
let kpopupViewTag = 22222
let kOverlayViewTag = 22222

let kSourceViewTag2 = 111112
let kpopupViewTag2 = 222221
let kOverlayViewTag2 = 222221

var kpopupViewController:UInt8 = 0
var kpopupBackgroundView:UInt8 = 1

var kpopupViewController2:UInt8 = 2
var kpopupBackgroundView2:UInt8 = 3

let kpopupAnimationDuration = 0.35
let kSLViewDismissKey = "kSLViewDismissKey"
let kSLViewDismissKey2 = "kSLViewDismissKey2"

extension UIViewController {
    var popupBackgroundView:UIView? {
        get {
            return objc_getAssociatedObject(self, &kpopupBackgroundView) as? UIView
        }
        set(newValue) {
            objc_setAssociatedObject(self, &kpopupBackgroundView, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    var popupViewController:UIViewController? {
        get {
            return objc_getAssociatedObject(self, &kpopupViewController) as? UIViewController
        }
        set(newValue) {
            objc_setAssociatedObject(self, &kpopupViewController, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    var dismissedCallback:UIViewController? {
        get {
            return objc_getAssociatedObject(self, kSLViewDismissKey) as? UIViewController
        }
        set(newValue) {
            objc_setAssociatedObject(self, kSLViewDismissKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    
    var popupBackgroundView2:UIView? {
        get {
            return objc_getAssociatedObject(self, &kpopupBackgroundView2) as? UIView
        }
        set(newValue) {
            objc_setAssociatedObject(self, &kpopupBackgroundView2, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    var popupViewController2:UIViewController? {
        get {
            return objc_getAssociatedObject(self, &kpopupViewController2) as? UIViewController
        }
        set(newValue) {
            objc_setAssociatedObject(self, &kpopupViewController2, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    var dismissedCallback2:UIViewController? {
        get {
            return objc_getAssociatedObject(self, kSLViewDismissKey2) as? UIViewController
        }
        set(newValue) {
            objc_setAssociatedObject(self, kSLViewDismissKey2, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func presentpopupViewController(popupViewController: UIViewController, animationType:SLpopupViewAnimationType, completion:() -> Void) {
        let sourceView:UIView = self.getTopView()
        self.popupViewController = popupViewController
        let popupView:UIView = popupViewController.view
        sourceView.tag = kSourceViewTag
        popupView.autoresizingMask = [.FlexibleTopMargin,.FlexibleLeftMargin,.FlexibleRightMargin,.FlexibleBottomMargin]
        popupView.tag = kpopupViewTag
        if(sourceView.subviews.contains(popupView)) {
            return
        }
        popupView.layer.shadowPath = UIBezierPath(rect: popupView.bounds).CGPath
        popupView.layer.masksToBounds = false
//        popupView.layer.shadowOffset = CGSizeMake(5, 5)
//        popupView.layer.shadowRadius = 5
//        popupView.layer.shadowOpacity = 0.5
        popupView.layer.shouldRasterize = true
        popupView.layer.rasterizationScale = UIScreen.mainScreen().scale
        
        let overlayView:UIView = UIView(frame: sourceView.bounds)
        overlayView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        overlayView.tag = kOverlayViewTag
        overlayView.backgroundColor = UIColor.clearColor()
        
        self.popupBackgroundView = UIView(frame: sourceView.bounds)
        self.popupBackgroundView!.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.popupBackgroundView!.backgroundColor = UIColor.blackColor()
        self.popupBackgroundView?.alpha = 0.0
        if let _ = self.popupBackgroundView {
            overlayView.addSubview(self.popupBackgroundView!)
        }
        //Background is button
        let dismissButton: UIButton = UIButton(type: .Custom)
//        dismissButton.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
//        dismissButton.backgroundColor = UIColor.clearColor()
//        dismissButton.frame = sourceView.bounds
//        overlayView.addSubview(dismissButton)
        
        popupView.alpha = 0.0
        overlayView.addSubview(popupView)
        sourceView.addSubview(overlayView)
        
        dismissButton.addTarget(self, action: #selector(UIViewController.btnDismissViewControllerWithAnimation(_:)), forControlEvents: .TouchUpInside)
        switch animationType {
        case .BottomTop, .BottomBottom,.TopTop,.TopBottom, .LeftLeft, .LeftRight,.RightLeft, .RightRight:
            dismissButton.tag = animationType.rawValue
            print("slider1")
            self.slideView(popupView, sourceView: sourceView, overlayView: overlayView, animationType: animationType)
            
        default:
            dismissButton.tag = SLpopupViewAnimationType.Fade.rawValue
            self.fadeView(popupView, sourceView: sourceView, overlayView: overlayView)
        }
        
    }
    
    func presentpopupViewController2(popupViewController: UIViewController, animationType:SLpopupViewAnimationType, completion:() -> Void) {
        let sourceView:UIView = self.getTopView2()
        self.popupViewController2 = popupViewController
        let popupView:UIView = popupViewController2!.view
        sourceView.tag = kSourceViewTag2
        popupView.autoresizingMask = [.FlexibleTopMargin,.FlexibleLeftMargin,.FlexibleRightMargin,.FlexibleBottomMargin]
        popupView.tag = kpopupViewTag2
        if(sourceView.subviews.contains(popupView)) {
            return
        }
        popupView.layer.shadowPath = UIBezierPath(rect: popupView.bounds).CGPath
        popupView.layer.masksToBounds = false
//        popupView.layer.shadowOffset = CGSizeMake(5, 5)
//        popupView.layer.shadowRadius = 5
//        popupView.layer.shadowOpacity = 0.5
        popupView.layer.shouldRasterize = true
        popupView.layer.rasterizationScale = UIScreen.mainScreen().scale
        
        let overlayView:UIView = UIView(frame: sourceView.bounds)
        overlayView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        overlayView.tag = kOverlayViewTag2
        overlayView.backgroundColor = UIColor.clearColor()
        
        self.popupBackgroundView2 = UIView(frame: sourceView.bounds)
        self.popupBackgroundView2!.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.popupBackgroundView2!.backgroundColor = UIColor.blackColor()
        self.popupBackgroundView2?.alpha = 0.0
        if let _ = self.popupBackgroundView2 {
            overlayView.addSubview(self.popupBackgroundView2!)
        }
        //Background is button
        let dismissButton: UIButton = UIButton(type: .Custom)
//                dismissButton.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
//                dismissButton.backgroundColor = UIColor.clearColor()
//                dismissButton.frame = sourceView.bounds
//                overlayView.addSubview(dismissButton)
        
        popupView.alpha = 0.0
        overlayView.addSubview(popupView)
        sourceView.addSubview(overlayView)
        
        dismissButton.addTarget(self, action: #selector(UIViewController.btnDismissViewControllerWithAnimation2(_:)), forControlEvents: .TouchUpInside)
        switch animationType {
        case .BottomTop, .BottomBottom,.TopTop,.TopBottom, .LeftLeft, .LeftRight,.RightLeft, .RightRight:
            dismissButton.tag = animationType.rawValue
            print("slider1")
            self.slideView2(popupView, sourceView: sourceView, overlayView: overlayView, animationType: animationType)
            
        default:
            dismissButton.tag = SLpopupViewAnimationType.Fade.rawValue
            self.fadeView2(popupView, sourceView: sourceView, overlayView: overlayView)
        }
        
    }

    
    
    func slideView(popupView: UIView, sourceView:UIView, overlayView:UIView, animationType: SLpopupViewAnimationType) {
        let sourceSize: CGSize = sourceView.bounds.size
        let popupSize: CGSize = popupView.bounds.size
        var popupStartRect:CGRect
        switch animationType {
        case .BottomTop, .BottomBottom:
            popupStartRect = CGRectMake((sourceSize.width - popupSize.width)/2, sourceSize.height, popupSize.width, popupSize.height)
        case .LeftLeft, .LeftRight:
            popupStartRect = CGRectMake(-sourceSize.width, (sourceSize.height - popupSize.height)/2, popupSize.width, popupSize.height)
        case .TopTop, .TopBottom:
            popupStartRect = CGRectMake((sourceSize.width - popupSize.width)/2, -sourceSize.height, popupSize.width, popupSize.height)
        default:
            popupStartRect = CGRectMake(sourceSize.width, (sourceSize.height - popupSize.height)/2, popupSize.width, popupSize.height)
        }
        //Change The View Location
        
        let popupEndRect:CGRect = CGRectMake((sourceSize.width - popupSize.width)/2, (sourceSize.height - popupSize.height)/2 - 100, popupSize.width, popupSize.height)
        popupView.frame = popupStartRect
        popupView.alpha = 1.0
        UIView.animateWithDuration(kpopupAnimationDuration, animations: { () -> Void in
            self.popupViewController?.viewWillAppear(false)
            self.popupBackgroundView?.alpha = 0.5
            popupView.frame = popupEndRect
            }) { (finished) -> Void in
                self.popupViewController?.viewDidAppear(false)
        }
        
    }
    
    func slideView2(popupView: UIView, sourceView:UIView, overlayView:UIView, animationType: SLpopupViewAnimationType) {
        let sourceSize: CGSize = sourceView.bounds.size
        let popupSize: CGSize = popupView.bounds.size
        var popupStartRect:CGRect
        switch animationType {
        case .BottomTop, .BottomBottom:
            popupStartRect = CGRectMake((sourceSize.width - popupSize.width)/2, sourceSize.height, popupSize.width, popupSize.height)
        case .LeftLeft, .LeftRight:
            popupStartRect = CGRectMake(-sourceSize.width, (sourceSize.height - popupSize.height)/2, popupSize.width, popupSize.height)
        case .TopTop, .TopBottom:
            popupStartRect = CGRectMake((sourceSize.width - popupSize.width)/2, -sourceSize.height, popupSize.width, popupSize.height)
        default:
            popupStartRect = CGRectMake(sourceSize.width, (sourceSize.height - popupSize.height)/2, popupSize.width, popupSize.height)
        }
        let popupEndRect:CGRect = CGRectMake((sourceSize.width - popupSize.width)/2, (sourceSize.height - popupSize.height)/2 - 90, popupSize.width, popupSize.height)
        popupView.frame = popupStartRect
        popupView.alpha = 1.0
        UIView.animateWithDuration(kpopupAnimationDuration, animations: { () -> Void in
            self.popupViewController2?.viewWillAppear(false)
            self.popupBackgroundView2?.alpha = 0.5
            popupView.frame = popupEndRect
        }) { (finished) -> Void in
            self.popupViewController2?.viewDidAppear(false)
        }
        
    }

    
    func slideViewOut(popupView: UIView, sourceView:UIView, overlayView:UIView, animationType: SLpopupViewAnimationType) {
        let sourceSize: CGSize = sourceView.bounds.size
        let popupSize: CGSize = popupView.bounds.size
        var popupEndRect:CGRect
        switch animationType {
        case .BottomTop, .TopTop:
            popupEndRect = CGRectMake((sourceSize.width - popupSize.width)/2, -popupSize.height, popupSize.width, popupSize.height)
        case .BottomBottom, .TopBottom:
            popupEndRect = CGRectMake((sourceSize.width - popupSize.width)/2, popupSize.height, popupSize.width, popupSize.height)
        case .LeftRight, .RightRight:
            popupEndRect = CGRectMake(sourceSize.width, popupView.frame.origin.y, popupSize.width, popupSize.height)
        default:
            popupEndRect = CGRectMake(-popupSize.width, popupView.frame.origin.y, popupSize.width, popupSize.height)
        }
        
        UIView.animateWithDuration(0.2, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
                self.popupBackgroundView?.backgroundColor = UIColor.clearColor()
            }) { (finished) -> Void in
                UIView.animateWithDuration(kpopupAnimationDuration, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
                    self.popupViewController?.viewWillDisappear(false)
                    popupView.frame = popupEndRect
                    }) { (finished) -> Void in
                        popupView.removeFromSuperview()
                        overlayView.removeFromSuperview()
                        self.popupViewController?.viewDidDisappear(false)
                        self.popupViewController = nil
                }
        }
        
        
        
    }
    func slideViewOut2(popupView: UIView, sourceView:UIView, overlayView:UIView, animationType: SLpopupViewAnimationType) {
        let sourceSize: CGSize = sourceView.bounds.size
        let popupSize: CGSize = popupView.bounds.size
        var popupEndRect:CGRect
        switch animationType {
        case .BottomTop, .TopTop:
            popupEndRect = CGRectMake((sourceSize.width - popupSize.width)/2, -popupSize.height, popupSize.width, popupSize.height)
        case .BottomBottom, .TopBottom:
            popupEndRect = CGRectMake((sourceSize.width - popupSize.width)/2, popupSize.height, popupSize.width, popupSize.height)
        case .LeftRight, .RightRight:
            popupEndRect = CGRectMake(sourceSize.width, popupView.frame.origin.y, popupSize.width, popupSize.height)
        default:
            popupEndRect = CGRectMake(-popupSize.width, popupView.frame.origin.y, popupSize.width, popupSize.height)
        }
        
        UIView.animateWithDuration(0.2, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
            self.popupBackgroundView2?.backgroundColor = UIColor.clearColor()
        }) { (finished) -> Void in
            UIView.animateWithDuration(kpopupAnimationDuration, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
                self.popupViewController2?.viewWillDisappear(false)
                popupView.frame = popupEndRect
            }) { (finished) -> Void in
                popupView.removeFromSuperview()
                overlayView.removeFromSuperview()
                self.popupViewController2?.viewDidDisappear(false)
                self.popupViewController2 = nil
            }
        }
        
        
        
    }

    func fadeView(popupView: UIView, sourceView:UIView, overlayView:UIView) {
        let sourceSize: CGSize = sourceView.bounds.size
        let popupSize: CGSize = popupView.bounds.size
        popupView.frame = CGRectMake((sourceSize.width - popupSize.width)/2,
                                                (sourceSize.height - popupSize.height)/2,
                                                popupSize.width,
                                                popupSize.height)
        popupView.alpha = 0.0
        
        UIView.animateWithDuration(kpopupAnimationDuration, animations: { () -> Void in
            self.popupViewController!.viewWillAppear(false)
            self.popupBackgroundView!.alpha = 0.5
            popupView.alpha = 1.0
        }) { (finished) -> Void in
           self.popupViewController?.viewDidAppear(false)
        }
        
    }
    func fadeView2(popupView: UIView, sourceView:UIView, overlayView:UIView) {
        let sourceSize: CGSize = sourceView.bounds.size
        let popupSize: CGSize = popupView.bounds.size
        popupView.frame = CGRectMake((sourceSize.width - popupSize.width)/2,
                                     (sourceSize.height - popupSize.height)/2,
                                     popupSize.width,
                                     popupSize.height)
        popupView.alpha = 0.0
        
        UIView.animateWithDuration(kpopupAnimationDuration, animations: { () -> Void in
            self.popupViewController2!.viewWillAppear(false)
            self.popupBackgroundView2!.alpha = 0.5
            popupView.alpha = 1.0
        }) { (finished) -> Void in
            self.popupViewController2?.viewDidAppear(false)
        }
        
    }

    func fadeViewOut(popupView: UIView, sourceView:UIView, overlayView:UIView) {
        UIView.animateWithDuration(kpopupAnimationDuration, animations: { () -> Void in
            self.popupViewController?.viewDidDisappear(false)
            self.popupBackgroundView?.alpha = 0.0
            popupView.alpha = 0.0
        }) { (finished) -> Void in
            popupView.removeFromSuperview()
            overlayView.removeFromSuperview()
            self.popupViewController?.viewDidDisappear(false)
            self.popupViewController = nil
        }
        
    }
    func fadeViewOut2(popupView: UIView, sourceView:UIView, overlayView:UIView) {
        UIView.animateWithDuration(kpopupAnimationDuration, animations: { () -> Void in
            self.popupViewController2?.viewDidDisappear(false)
            self.popupBackgroundView2?.alpha = 0.0
            popupView.alpha = 0.0
        }) { (finished) -> Void in
            popupView.removeFromSuperview()
            overlayView.removeFromSuperview()
            self.popupViewController2?.viewDidDisappear(false)
            self.popupViewController2 = nil
        }
        
    }

    func btnDismissViewControllerWithAnimation(btnDismiss : UIButton) {
        let animationType:SLpopupViewAnimationType = SLpopupViewAnimationType(rawValue: btnDismiss.tag)!
        switch animationType {
        case .BottomTop, .BottomBottom, .TopTop, .TopBottom, .LeftLeft, .LeftRight, .RightLeft, .RightRight:
            self.dismissPopupViewController(animationType)
        default:
            self.dismissPopupViewController(SLpopupViewAnimationType.Fade)
        }
    }
    func btnDismissViewControllerWithAnimation2(btnDismiss : UIButton) {
        let animationType:SLpopupViewAnimationType = SLpopupViewAnimationType(rawValue: btnDismiss.tag)!
        switch animationType {
        case .BottomTop, .BottomBottom, .TopTop, .TopBottom, .LeftLeft, .LeftRight, .RightLeft, .RightRight:
            self.dismissPopupViewController2(animationType)
        default:
            self.dismissPopupViewController2(SLpopupViewAnimationType.Fade)
        }
    }

    func getTopView() -> UIView {
        var recentViewController:UIViewController = self
        if let _ = recentViewController.parentViewController {
           recentViewController = recentViewController.parentViewController!
        }
        return recentViewController.view
    }
    func getTopView2() -> UIView {
        var recentViewController:UIViewController = self
        if let _ = recentViewController.parentViewController {
            recentViewController = recentViewController.parentViewController!
        }
        return recentViewController.view
    }

    
    func dismissPopupViewController(animationType: SLpopupViewAnimationType) {
        let sourceView:UIView = self.getTopView()
        let popupView:UIView = sourceView.viewWithTag(kpopupViewTag)!
        let overlayView:UIView = sourceView.viewWithTag(kOverlayViewTag)!
        switch animationType {
        case .BottomTop, .BottomBottom, .TopTop, .TopBottom, .LeftLeft, .LeftRight, .RightLeft, .RightRight:
            self.slideViewOut(popupView, sourceView: sourceView, overlayView: overlayView, animationType: animationType)
        default:
            fadeViewOut(popupView, sourceView: sourceView, overlayView: overlayView)
            
            
        }
    }
    func dismissPopupViewController2(animationType: SLpopupViewAnimationType) {
        let sourceView:UIView = self.getTopView2()
        let popupView:UIView = sourceView.viewWithTag(kpopupViewTag2)!
        let overlayView:UIView = sourceView.viewWithTag(kOverlayViewTag2)!
        switch animationType {
        case .BottomTop, .BottomBottom, .TopTop, .TopBottom, .LeftLeft, .LeftRight, .RightLeft, .RightRight:
            self.slideViewOut2(popupView, sourceView: sourceView, overlayView: overlayView, animationType: animationType)
        default:
            fadeViewOut2(popupView, sourceView: sourceView, overlayView: overlayView)
            
            
        }
    }

}