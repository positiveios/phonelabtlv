//
//  roundBackgroundWhite.swift
//  PushSpots
//
//  Created by Social Sky Team on 10/19/15.
//  Copyright © 2015 Social Sky Team. All rights reserved.
//

import UIKit

class roundBackgroundWhite: UIButton {

    func initialize(){
        
      //  self.backgroundColor = UIColor(red: 129.0/255.0, green: 196.0/255.0, blue: 16.0/255.0, alpha: 1.0)
        
        self.layer.cornerRadius = self.frame.size.width / 10
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowColor = UIColor.blackColor().CGColor
        //self.layer.borderColor = UIColor.blackColor().CGColor
    
        
        
        
    }
    //From Code
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    //From IB
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

}
