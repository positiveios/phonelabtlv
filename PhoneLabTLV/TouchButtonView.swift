//
//  TouchButtonView.swift
//  PhoneLabTLV
//
//  Created by PositiveApps on 05/07/2016.
//  Copyright © 2016 PositiveApps. All rights reserved.
//

import UIKit

class TouchButtonView: UIView {

    func initialize(){
        
//        self.layer.cornerRadius = self.frame.size.width / 1.6
//        self.clipsToBounds = true
//        
//        
//        self.layer.borderWidth = 2.0
//        self.layer.borderColor = UIColor.redColor().CGColor
        
        
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
        
        self.layer.borderColor = UIColor.redColor().CGColor
        self.layer.borderWidth = 2.0
        
    }
    //From Code
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    //From IB
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

}
