//
//  TouchViewController.swift
//  PhoneLabTLV
//
//  Created by PositiveApps on 07/05/2016.
//  Copyright © 2016 PositiveApps. All rights reserved.
//

import UIKit

class TouchViewController2: UIViewController {

    @IBOutlet weak var touchBtn1: UIButton!
    @IBOutlet weak var touchBtn2: UIButton!
    @IBOutlet weak var touchBtn3: UIButton!
    @IBOutlet weak var touchBtn4: UIButton!
    @IBOutlet weak var touchBtn5: UIButton!
    
    @IBAction func touchBtn2hiddentruetouchBtn1Action(sender: UIButton) {
        
        touchBtn1.hidden = true
        touchBtn2.hidden = false
        
    }
    @IBAction func touchBtn2hiddentruetouchBtn2Action(sender: UIButton) {
        
        touchBtn2.hidden = true
        touchBtn3.hidden = false
        
    }
    @IBAction func touchBtn2hiddentruetouchBtn3Action(sender: UIButton) {
        
        touchBtn3.hidden = true
        touchBtn4.hidden = false
        
    }
    @IBAction func touchBtn2hiddentruetouchBtn4Action(sender: UIButton) {
        
        touchBtn4.hidden = true
        touchBtn5.hidden = false
        
    }

    @IBAction func touchBtn2hiddentruetouchBtn5Action(sender: UIButton) {
        
        touchBtn5.hidden = true

        
        let alert = UIAlertView()
        alert.title = "בדיקת נגיעה"
        alert.message = "בדיקת נגיעה עברה בהצלחה"
        alert.addButtonWithTitle("אישור")
        alert.show()
        
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }

        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        touchBtn2.hidden = true
        touchBtn3.hidden = true
        touchBtn4.hidden = true
        touchBtn5.hidden = true

        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
