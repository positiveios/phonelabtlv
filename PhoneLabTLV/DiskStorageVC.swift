//
//  DiskStorageVC.swift
//  PhoneLabTLV
//
//  Created by PositiveApps on 5/2/16.
//  Copyright © 2016 PositiveApps. All rights reserved.
//


import UIKit
import SystemConfiguration

class DiskStorageVC: UIViewController {
    
    @IBOutlet var usedLabel: UILabel!
    @IBOutlet var freeLabel: UILabel!
    @IBOutlet weak var iphone: UILabel!
    @IBOutlet weak var iphoneName: UILabel!
    @IBOutlet weak var iphoneV: UILabel!
    
    @IBOutlet weak var udidLabel: UILabel!
    var diskUsedView:UIView?
    
    
    
    @IBAction func backBtnAction(sender: UIButton) {
        
        
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
        
        
        
    }
    

      override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //print(self.filterUDID(UIDevice.currentDevice().identifierForVendor!.UUIDString))

//        
//        do {
//            let reachability: Reachability = try Reachability.reachabilityForInternetConnection()
//            
//            switch reachability.currentReachabilityStatus{
//            case .ReachableViaWiFi:
//                print("Connected With wifi")
//            case .ReachableViaWWAN:
//                print("Connected With Cellular network(3G/4G)")
//            case .NotReachable:
//                print("Not Connected")
//            }
//        }
//        catch let error as NSError{
//            print(error.debugDescription)
//        }
//
        
        self.udidLabel.text = UIDevice.currentDevice().identifierForVendor!.UUIDString
        
        self.iphone.text = UIDevice.currentDevice().modelName
        
        self.iphoneName.text = UIDevice.currentDevice().name
        
        self.iphoneV.text = UIDevice.currentDevice().systemVersion
        
      
        LocalDataManager.sharedInstance().setDataVC(true)
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.updateDiskStatus()
    }
    
    
    
    func filterUDID(url:String) -> String{
        
        let dateString = url.stringByReplacingOccurrencesOfString("-", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        return dateString
        
        
        
    }

      // MARK: update disk space status
    func updateDiskStatus() {
        usedLabel!.text = String(format:NSLocalizedString("%@", comment: ""), DiskStatus.usedDiskSpace)
        freeLabel!.text = String(format:NSLocalizedString("%@", comment: ""), DiskStatus.freeDiskSpace)
        
    }
    
}

