//
//  MyPopupViewController.swift
//  SLPopupViewControllerDemo
//
//  Created by Nguyen Duc Hoang on 9/13/15.
//  Copyright © 2015 Nguyen Duc Hoang. All rights reserved.
//

import UIKit

protocol ConfirmationPopDelegate {
    func pressOK(sender: ConfirmationPop)
    func pressCancel(sender: ConfirmationPop)

    
}
class ConfirmationPop: UIViewController {
    var delegate:ConfirmationPopDelegate?
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBAction func btnOK(sender:UIButton) {
        
        
        
        self.delegate?.pressOK(self)
        
    }
    
    @IBAction func btnCancel(sender:UIButton) {
        self.delegate?.pressCancel(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        self.titleLabel.text = LocalDataManager.sharedInstance().title

        self.messageLabel.text = LocalDataManager.sharedInstance().message
        
        self.iconImage.image = LocalDataManager.sharedInstance().logo
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
        /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
