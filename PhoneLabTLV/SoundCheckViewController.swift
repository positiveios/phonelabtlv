//
//  SoundCheckViewController.swift
//  PhoneLabTLV
//
//  Created by PositiveApps on 07/05/2016.
//  Copyright © 2016 PositiveApps. All rights reserved.
//

import UIKit

class SoundCheckViewController: UIViewController,UITextFieldDelegate,ConfirmationPopDelegate,SuccessPopDelegate {

    
    let player = SoundsEffect()

    
    @IBAction func backBtnAction(sender: UIButton) {
        
        
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
        
        
        
    }
    

    
    @IBOutlet weak var numberTextField: UITextField!
    @IBAction func playSoundAction(sender: UIButton) {
        
        self.player.playScanBeep()

        
    }
    @IBAction func okBtnAction(sender: UIButton) {
        
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.displayViewController(.BottomTop)

        self.view.backgroundColor = UIColor.init(colorCodeInHex: "1e9e49")

        
        numberTextField.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        if (textField == numberTextField)
        {
            let newString = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
            let components = newString.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet)
            
            let decimalString = components.joinWithSeparator("") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.characterAtIndex(0) == (1 as unichar)
            
            if length == 0 || (length > 4 && !hasLeadingOne) || length > 4
            {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                
                return (newLength > 4) ? false : true
            }
            var index = 0 as Int
            let formattedString = NSMutableString()
            
//            if hasLeadingOne
//            {
//                formattedString.appendString("1 ")
//                index += 1
//            }
//            if (length - index) > 3
//            {
//                let areaCode = decimalString.substringWithRange(NSMakeRange(index, 3))
//                formattedString.appendFormat("%@-", areaCode)
//                index += 3
//            }
//            if length - index > 3
//            {
//                let prefix = decimalString.substringWithRange(NSMakeRange(index, 3))
//                formattedString.appendFormat("%@-", prefix)
//                index += 3
//            }
            
            if length == 4{
                
               // scrollView.setContentOffset(CGPointMake(0, 0), animated: true)
                self.numberTextField.resignFirstResponder()
                
                
                
            }
            
            let remainder = decimalString.substringFromIndex(index)
            formattedString.appendString(remainder)
            textField.text = formattedString as String
            print(self.numberTextField.text!)
            
            self.soundCheck(self.numberTextField.text!)
            return false
        }
        else
        {
            
            
            print(self.numberTextField.text!)
            
            self.soundCheck(self.numberTextField.text!)

            return true
        }
    }

    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        self.soundCheck(textField.text!)
        
        return true
    }

    
    func displayViewController(animationType: SLpopupViewAnimationType) {
        
        let image = UIImage(named: "speaker_before_btn")
        
        LocalDataManager.sharedInstance().setupTitle("בדיקת רמקול", M:  "הזן את ארבע הספרות שיושמעו ברמקול",image: image!)
        
        let myPopupViewController:ConfirmationPop = ConfirmationPop(nibName:"ConfirmationPop", bundle: nil)
        myPopupViewController.delegate = self
        self.presentpopupViewController(myPopupViewController, animationType: animationType, completion: { () -> Void in
            
        })
    }
    
    
    
    func displayViewControllerSuccess(animationType: SLpopupViewAnimationType) {
        
        let image = UIImage(named: "speaker_after_btn")
        
        LocalDataManager.sharedInstance().setupTitle2("הבדיקה הושלמה בהצלחה!", image: image!)
        let myPopupViewController:SuccessPop = SuccessPop(nibName:"SuccessPop", bundle: nil)
        myPopupViewController.delegate = self
        self.presentpopupViewController2(myPopupViewController, animationType: animationType, completion: { () -> Void in
            
        })
    }
    
    func pressOK(sender: ConfirmationPop) {
        
        self.dismissPopupViewController(.Fade)
        
        self.numberTextField.becomeFirstResponder()

        //        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("tryAgain")
        //        self.presentViewController(vc!, animated: true, completion: nil)
        
        
    }
    
    func pressCancel(sender: ConfirmationPop) {
        self.dismissPopupViewController(.Fade)
        
        
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
        
        
    }
    
    
    
    func pressOKSuccess(sender: SuccessPop) {
        
        
        LocalDataManager.sharedInstance().setSoundVC(true)
        
        self.dismissPopupViewController2(.Fade)
        
        
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
        
        
    }
    
    func pressCancelSuccess(sender: SuccessPop) {
        self.dismissPopupViewController(.Fade)
        
        
        
        
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func soundCheck(text : NSString){
        
        if(self.numberTextField.text == "4864"){
            
            
            self.displayViewControllerSuccess(.BottomTop)
            
        }else if(text.length >= 4){
            
            let alert = UIAlertView()
                        alert.title = "בדיקת שמע"
                        alert.message = "בדיקת שמע נכשלה אנא נסה שנית"
                        alert.addButtonWithTitle("אישור")
                        alert.show()
            
            self.numberTextField.text = ""
            self.numberTextField.becomeFirstResponder()
            
            
        }

        
    }

}
