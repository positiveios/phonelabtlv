//
//  WebViewPayViewController.swift
//  PhoneLabTLV
//
//  Created by PositiveApps on 14/08/2016.
//  Copyright © 2016 PositiveApps. All rights reserved.
// https://plando.co.il/lead_forms/pay_and_invoice/?txn_type=express_checkout&receiver_id=5aef48f93ea02372dc9d7f0b1360039b&last_name=ישראל&first_name=ישראלי&payer_email=israel@israeli.com&item_name=ביטוח_לטלפון&mc_gross=125&tax=12&mc_currency=ILS&success_url=http://your_success_url.com&failure_url=http://your_failure_url.com

import UIKit

class WebViewPayViewController: UIViewController ,UIWebViewDelegate{

    var animationTypes: [RPLoadingAnimationType] = [
        .RotatingCircle,
        .SpininngDot,
        .LineScale,
        .DotTrianglePath,
        .DotSpinningLikeSkype,
        .FunnyDotsA
    ]
    
    
    
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
//        if let requestUrl = NSURL(string: "https://plando.co.il/lead_forms/pay_and_invoice/?txn_type=express_checkout&receiver_id=5aef48f93ea02372dc9d7f0b1360039b&last_name=ישראל&first_name=ישראלי&payer_email=israel@israeli.com&item_name=ביטוח_לטלפון&mc_gross=125&tax=12&mc_currency=ILS&success_url=http://your_success_url.com&failure_url=http://your_failure_url.com") {
//            UIApplication.sharedApplication().openURL(requestUrl)
//        }
//        
//        webView.delegate = self
//        
//      
        let stringUrl = "https://plando.co.il/lead_forms/pay_and_invoice/?txn_type=express_checkout&receiver_id=5aef48f93ea02372dc9d7f0b1360039b&last_name=ישראל&first_name=ישראלי&payer_email=israel@israeli.com&item_name=ביטוח_לטלפון&mc_gross=125&tax=12&mc_currency=ILS&success_url=http://your_success_url.com&failure_url=http://your_failure_url.com"//firstName.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        
        //let stringUrl = "https://www.google.com/"
        
        let requestURL = NSURL(string:stringUrl.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
        let request = NSURLRequest(URL: requestURL!)
        webView.loadRequest(request)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        
        self.loading(false)
        
        let currentURL : NSString = (webView.request?.URL!.absoluteString)!

        print(currentURL)
        
    }
    func webViewDidStartLoad(webView: UIWebView) {
        
        self.loading(true)
        
        
    }
    
    func loading(boolLoad : Bool){
        
        
        if(boolLoad){
            
            let ll = UIView(frame: CGRect(x: self.view.frame.size.width / 2 - 50, y: self.view.frame.size.height / 2 - 50, width: 100, height: 100))
            
            
            self.view.userInteractionEnabled = false
            
            
            let cellSize = ll.bounds.size
            let animationFrame = CGRect(origin: CGPointZero, size: cellSize)
            
            let animationView = RPLoadingAnimationView(
                frame: animationFrame,
                type: animationTypes[1],
                color: UIColor(red: 223.0/255.0, green: 189.0/255.0, blue: 43.0/255.0, alpha: 1.0),
                size: cellSize
            )
            
            ll.addSubview(animationView)
            animationView.setupAnimation()
            ll.tag = 100
            ll.userInteractionEnabled = true
            
            self.view.addSubview(ll)
            ll.hidden = false
            
            
        }else{
            
            if let viewWithTag = self.view.viewWithTag(100) {
                viewWithTag.removeFromSuperview()
                self.view.userInteractionEnabled = true
                
                
            }else{
                print("No!")
                self.view.userInteractionEnabled = true
                
            }
            
            
            
            
        }
        
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
