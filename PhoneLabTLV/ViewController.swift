//
//  ViewController.swift
//  PhoneLabTLV
//
//  Created by PositiveApps on 5/2/16.
//  Copyright © 2016 PositiveApps. All rights reserved.
//


//צבעים:
//
//ירוק רקע - 1e9e49
//
//ירוק רקע כותרות (בר עליון) - 169340
//
//ירוק כהה - 1f7134
//
//אדום - 8b0900
//
//אדום בהיר - cc928e
//





import UIKit
import CoreMotion
import AVFoundation
import AudioToolbox

extension UIColor{
    
    public convenience init?(colorCodeInHex: String, alpha: Float = 1.0){
        
        var filterColorCode:String =  colorCodeInHex.stringByReplacingOccurrencesOfString("#", withString: "")
        
        if  filterColorCode.characters.count != 6 {
            self.init(red: 0.0, green: 0.0, blue: 0.0, alpha: CGFloat(alpha))
            return
        }
        
        filterColorCode = filterColorCode.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).uppercaseString
        
        var range = Range(start: filterColorCode.startIndex.advancedBy(0), end: filterColorCode.startIndex.advancedBy(2))
        let rString = filterColorCode.substringWithRange(range)
        
        range = Range(start: filterColorCode.startIndex.advancedBy(2), end: filterColorCode.startIndex.advancedBy(4))
        let gString = filterColorCode.substringWithRange(range)
        
        
        range = Range(start: filterColorCode.startIndex.advancedBy(4), end: filterColorCode.startIndex.advancedBy(6))
        let bString = filterColorCode.substringWithRange(range)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        NSScanner(string: rString).scanHexInt(&r)
        NSScanner(string: gString).scanHexInt(&g)
        NSScanner(string: bString).scanHexInt(&b)
        
        
        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(alpha))
        return
    }
}



public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 where value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1":
            return "iPhone 5"
        case  "iPhone5,2":
            return "iPhone 5,2"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}


class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    
    
    var menuArray = [String]()
    var menuImageArray = [UIImage]()
    var checkImageArray = [UIImage]()
    var colorsArray = [UIColor]()
    
    var enImage = UIImage(named: "buy_insurance_green_btn")
    var disImage = UIImage(named: "buy_insurance_grey_btn")

    
    @IBOutlet weak var VCtitleLabel: UILabel!
    @IBOutlet weak var menuTableView: UITableView!
    @IBAction func flashlightBtnAction(sender: AnyObject) {
        
       // self.toggleFlash()

      //  @IBAction func loadPDF() {
            
            let url = NSBundle.mainBundle().pathForResource("sample2", ofType: "pdf")!
            let document = PDFDocument(filePath: url, password: "")
            
            let pdf = PDFViewController(document: document)
            
            self.navigationController?.pushViewController(pdf, animated: true)
   //     }

        
    }

    @IBAction func viberBtnAction(sender: UIButton) {
        
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))

        
    }
    @IBOutlet weak var imagePic: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        UIDevice.currentDevice().batteryMonitoringEnabled = true
        //For Test
       // LocalDataManager.sharedInstance().setGyroVC(true)
       // LocalDataManager.sharedInstance().setSoundVC(true)

        //LocalDataManager.sharedInstance().setScreenVC(true)

    //    LocalDataManager.sharedInstance().setBrodcastVC(true)
       // LocalDataManager.sharedInstance().setDataVC(true)

        let test = UIDevice.currentDevice().batteryLevel
        let test2 = UIDevice.currentDevice().batteryState.rawValue
        
        print(test)
         print(test2)
        
        
        self.menuArray.append("מסך")
        self.menuArray.append("קליטה ושידור")
        self.menuArray.append("GYRO")
        self.menuArray.append("רמקול")
        self.menuArray.append("נתוני מכשיר")
        
        
        self.menuImageArray.append(UIImage(named: "screen_before_btn")!)
        self.menuImageArray.append(UIImage(named: "transmission_before_btn")!)
        self.menuImageArray.append(UIImage(named: "gyro_before_btn")!)
        self.menuImageArray.append(UIImage(named: "speaker_before_btn")!)
        self.menuImageArray.append(UIImage(named: "info_before_btn")!)
        
        
        self.checkImageArray.append(UIImage(named: "check_empty")!)
        self.checkImageArray.append(UIImage(named: "check_empty")!)
        self.checkImageArray.append(UIImage(named: "check_empty")!)
        self.checkImageArray.append(UIImage(named: "check_empty")!)
        self.checkImageArray.append(UIImage(named: "check_empty")!)
        
        
        self.colorsArray.append(UIColor.whiteColor())
        self.colorsArray.append(UIColor.whiteColor())
        self.colorsArray.append(UIColor.whiteColor())
        self.colorsArray.append(UIColor.whiteColor())
        self.colorsArray.append(UIColor.whiteColor())
        
        
        
        self.view.backgroundColor = UIColor.init(colorCodeInHex: "1e9e49")

       
        
        
        print(UIDevice.currentDevice().systemVersion)
        
        print(UIDevice.currentDevice().name)

        print(UIDevice.currentDevice().orientation.isPortrait)
        
        print(UIDevice.currentDevice().modelName)
        print(UIDevice.currentDevice().debugDescription)
        print(UIDevice.currentDevice().model)
        
        

        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tablerefresh()

        
        
        if(LocalDataManager.sharedInstance().screenVC){
            
            
            self.checkImageArray[0] = UIImage(named: "check_full")!
            
            self.menuImageArray[0] = UIImage(named: "screen_after_btn")!
            
            self.colorsArray[0] = UIColor.init(colorCodeInHex: "1f7134")!
            
            
            
        }
        
        if(LocalDataManager.sharedInstance().brodcastVC){
            
            
            self.checkImageArray[1] = UIImage(named: "check_full")!
            
            self.menuImageArray[1] = UIImage(named: "transmission_after_btn")!
            
            self.colorsArray[1] = UIColor.init(colorCodeInHex: "1f7134")!
            
        }
        
        if(LocalDataManager.sharedInstance().gyroVC){
            
            
            self.checkImageArray[2] = UIImage(named: "check_full")!
            
            self.menuImageArray[2] = UIImage(named: "gyro_after_btn")!
            
            self.colorsArray[2] = UIColor.init(colorCodeInHex: "1f7134")!
            
        }
        
        if(LocalDataManager.sharedInstance().soundVC){
            
            
            self.checkImageArray[3] = UIImage(named: "check_full")!
            
            self.menuImageArray[3] = UIImage(named: "speaker_after_btn")!
            
            self.colorsArray[3] = UIColor.init(colorCodeInHex: "1f7134")!
            
        }
        
        if(LocalDataManager.sharedInstance().dataVC){
            
            
            self.checkImageArray[4] = UIImage(named: "check_full")!
            
            self.menuImageArray[4] = UIImage(named: "info_after_btn")!
            
            self.colorsArray[4] = UIColor.init(colorCodeInHex: "1f7134")!
            
        }

        

    }
    
    
    func insurenceSuccess(){
        
        
        if(LocalDataManager.sharedInstance().brodcastVC && LocalDataManager.sharedInstance().soundVC && LocalDataManager.sharedInstance().screenVC && LocalDataManager.sharedInstance().dataVC && LocalDataManager.sharedInstance().gyroVC){
            
            
            
            
            
        }
        
    }
    
    
    func magnitudeFromAttitude(attitude: CMAttitude) -> Double {
        return sqrt(pow(attitude.roll, 2) + pow(attitude.yaw, 2) + pow(attitude.pitch, 2))
    }
    
    func tablerefresh()
    {
        dispatch_async(dispatch_get_main_queue(), {
            
            self.menuTableView.reloadData()
            return
        })
    }

    
    func toggleFlash() {
        let device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        if (device.hasTorch) {
            do {
                try device.lockForConfiguration()
                if (device.torchMode == AVCaptureTorchMode.On) {
                    device.torchMode = AVCaptureTorchMode.Off
                } else {
                    do {
                        try device.setTorchModeOnWithLevel(1.0)
                    } catch {
                        print(error)
                    }
                }
                device.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }
    
    
    
    func numberOfSectionsInTableView(tableView:UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        
            return menuArray.count + 1
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        
        if(indexPath.row < 5){
        
            let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! MenuTableViewCell
        

            
            cell.titleLabel.text = menuArray[indexPath.row]
        
            cell.pageLogoImage.image = menuImageArray[indexPath.row]
        
        
            cell.checkImage.image = checkImageArray[indexPath.row]
        
            
            cell.titleLabel.textColor = colorsArray[indexPath.row]
            
            
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCellWithIdentifier("insuranceCell", forIndexPath: indexPath) as! InsuranceTableViewCell
            
            
            if(LocalDataManager.sharedInstance().brodcastVC && LocalDataManager.sharedInstance().soundVC && LocalDataManager.sharedInstance().screenVC && LocalDataManager.sharedInstance().dataVC && LocalDataManager.sharedInstance().gyroVC){
                
                
                cell.insuranceBtn.enabled = true
                cell.insuranceBtn.setImage(self.enImage, forState: .Normal)
               // self.menuTableView.allowsSelection = true
                cell.VCLowTitleLabel.hidden = true
                
                self.VCtitleLabel.font = UIFont(name: "Rubik-Bold", size: 19)
                
                self.VCtitleLabel.text = "כל הבדיקות הושלמו בהצלחה!"
                
            }else{
                cell.insuranceBtn.enabled = false
                cell.insuranceBtn.setImage(self.disImage, forState: .Normal)
               // self.menuTableView.allowsSelection = false
                cell.VCLowTitleLabel.hidden = false

                
               
            }

            
            
            return cell
            
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        
        if(indexPath.row < 5){
            return 80.0
            
            
        }else{
            
            return 120.0
            
        }
        //Choose your custom row height
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if(indexPath.row == 0 && !LocalDataManager.sharedInstance().screenVC){
            
           self.performSegueWithIdentifier("ScreenVC", sender: self)

            
            
        }else if(indexPath.row == 1 && !LocalDataManager.sharedInstance().brodcastVC){
            
            self.performSegueWithIdentifier("BrodcastVC", sender: self)

            
            
        }else if(indexPath.row == 2 && !LocalDataManager.sharedInstance().gyroVC){
            
            self.performSegueWithIdentifier("GyroVC", sender: self)
            
            
            
        }else if(indexPath.row == 3 && !LocalDataManager.sharedInstance().soundVC){
            
            self.performSegueWithIdentifier("SoundVC", sender: self)
            
            
            
        }else if(indexPath.row == 4 && !LocalDataManager.sharedInstance().dataVC){
            
            self.performSegueWithIdentifier("InfoVC", sender: self)
            
            
            
        }



        
        
    }
    
       
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
//        if (segue.identifier == "ScreenVC") {
//            
//            if let viewController: DiskStorageVC = segue.destinationViewController as? DiskStorageVC {
//
//                
//                
//            }
//            
//        }
//    }
    
    // initial configuration
   
}

